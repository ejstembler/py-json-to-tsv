import json
import re
from datetime import datetime as dt

TAB = "\t"


class CcJsonMapper:
    """
    An example class of how to convert JSON data to tab-separated by streaming to STDOUT.
    """

    def __init__(self, filename, headers=['duration', 'content', 'ts']):
        """
        Constructs a new CcJsonMapper class.
        :param filename: The fully-qualified filename of the JSON file to load.
        :param headers: The headers to include in the tab-separated output
        """
        self.filename = filename
        self.headers = headers

    @staticmethod
    def normalize_content(s):
        """
        Normalizes string content. Modify this method to add any newly discovered normalization rules.
        :param s: The input string to normalize
        :return: The normalized string

        Example:

        >>> CcJsonMapper.normalize_content("<strong>This</strong> is an <i>example</i>.")
        'This is an example.'
        """
        result = re.sub("\t|\r|\n", ' ', s)  # Replace tabs, carriage returns, and newlines with a single space
        result = re.sub("<.*?>", '', result)  # Remove any html tags
        # Any other normalization rules should go here

        return result

    @staticmethod
    def ts_to_datetime(ts):
        """
        Converts a JavaScript timestamp value to a Python Datetime
        :param ts: The JavaScript timestamp value
        :return: A Python Datetime

        Example:

        >>> CcJsonMapper.ts_to_datetime(1520620096685)
        datetime.datetime(2018, 3, 9, 13, 28, 16, 685000)
        """
        return dt.fromtimestamp(ts / 1000.0)

    def map(self):
        """
        Opens the JSON file, loops through the cc array of dictionaries, normalizes them, and prints them to STDOUT tab-separated
        """
        # Load the JSON data
        data = json.load(open(self.filename))

        # Print the headers if present, and if there's data
        if self.headers and len(data):
            print(TAB.join(self.headers))

        # Loop through the cc array of dictionaries
        for item in data['cc']:
            # Normalize the data
            duration = str(item['duration'])
            content = CcJsonMapper.normalize_content(item['content'])
            ts = str(CcJsonMapper.ts_to_datetime(item['ts']))

            # Print to STDOUT tab-separated
            print(TAB.join([duration, content, ts]))


if __name__ == '__main__':
    import sys

    mapper = CcJsonMapper(sys.argv[1])  # The first argument is the filename
    mapper.map()
