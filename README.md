# Python JSON to TSV

This is an example of how to read a JavaScript Object Notation ([JSON](https://en.wikipedia.org/wiki/JSON)) file, and convert it to tab-separated values ([TSV](https://en.wikipedia.org/wiki/Tab-separated_values)) using standard out ([STDOUT](https://en.wikipedia.org/wiki/Standard_streams#Standard_output_(stdout))) streaming.

Refer to my [blog post]() for more background information.

## Files

|Name|Notes|
|---|---|
|[.gitignore](https://github.com/ejstembler/py-json-to-tsv/blob/master/.gitignore)|The [git ignore](https://git-scm.com/docs/gitignore) file.|
|[cc_json_mapper.py](https://github.com/ejstembler/py-json-to-tsv/blob/master/cc_json_mapper.py)|The main script which is a mapper.|
|[data.json](https://github.com/ejstembler/py-json-to-tsv/blob/master/data.json)|The closed captioned data in JSON format.|
|[data.vtt](https://github.com/ejstembler/py-json-to-tsv/blob/master/data.vtt)|The original data which is [WebVTT](https://www.w3.org/TR/webvtt1/) format. I converted it to the data.json file.|
|[README.md](https://github.com/ejstembler/py-json-to-tsv/blob/master/README.md)|This read me file.|

## Usage

From the terminal, type the following:

```bash
python cc_json_mapper.py data.json > data.tsv
```

### To run the doctests in verbose mode:

```bash
python -m doctest -v cc_json_mapper.py
```
